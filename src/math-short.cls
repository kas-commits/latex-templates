\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{math-short}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Personal info loader
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\type}{type}
\newcommand{\course}{course}
\newcommand{\name}{name}

\newcommand{\Type}[1]{\renewcommand{\type}{#1}}
\newcommand{\Course}[1]{\renewcommand{\course}{#1}}
\newcommand{\Name}[1]{\renewcommand{\name}{#1}}

\newcommand{\info}[3]{%
	\Name{#1}%
	\Type{#2}%
	\Course{\MakeUppercase{#3}}%
	}%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Misc Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\course\,--\,\type}
\author{\name}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Loading the Class and starting the formatting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[12pt,a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Page-Formatting packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc} % Font customization
\usepackage{parskip} % configures \parindent and \parskip
\usepackage{fancyhdr} % cofigures headers and footers
\usepackage{etoolbox} % logical conditionals like \ifstrempty
\usepackage[explicit]{titlesec} % formatting sections
\usepackage{lastpage} % gives you the lastpage command
\usepackage{geometry} % configures general page layout
\usepackage{hyperref} % to manage hyperlinks
\usepackage{graphicx,float} % for graphical inputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Package Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\graphicspath{ {./code/} {./images} }

\geometry{%
	headheight=18pt,%
	top=4.5em,%
	bottom=4.5em,%
	footnotesep=4em,%
	headsep=1.5em,%
	textwidth=42em,%
	}

\hypersetup{%
	colorlinks=true,%
	linkcolor=black,%
	filecolor=magenta,%
	urlcolor=blue,%
	}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Header, footer, and other configurations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Misc
\pagestyle{fancy}
\linespread{1.3}
\renewcommand{\theequation}{\thesection.\arabic{equation}}
%% Header
\fancyhead[L]{}
\fancyhead[C]{\large\MakeUppercase{\course}\,--\,\type}
\fancyhead[R]{\small\today}
%% Footer
\fancyfoot[L]{\name}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}
%% Header and Footer rulers
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Helper Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\seccon}[1]%
{%
  \ifstrempty{#1}%
  {}%
  {\space --\space}%
}%
\newcommand{\subseccon}[1]%
{%
  \ifstrempty{#1}%
  {}%
  {\space --\space}%
}%
\renewcommand{\thesubsection}{\alph{subsection}}
\renewcommand{\thesubsubsection}{\Roman{subsubsection}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Title Format
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titleformat%
{\section}%
% {\usefont{T1}{fvs}{b}{n}}%
{\bfseries}%
{\thesection\seccon{#1}}%
{0em}%
{#1}

\titleformat%
{\subsection}%
% {\usefont{T1}{fvm}{m}{n}\bfseries}%
{\bfseries}%
{(\thesubsection)\subseccon{#1}}%
{0ex}%
{#1}%

\titleformat%
{\subsubsection}%
% {\usefont{T1}{fvm}{m}{n}\bfseries\filcenter}%
{\bfseries\filcenter}%
{(\thesubsubsection)\subseccon{#1}}%
{0ex}%
{#1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Title Spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titlespacing%
{\section}%
{0ex}%
{2ex}%
{2ex}%

\titlespacing%
{\subsection}%
{0ex}%
{1ex}%
{1ex}%

\titlespacing%
{\subsubsection}%
{0ex}%
{1ex}%
{1ex}%

% vim:syn=tex
