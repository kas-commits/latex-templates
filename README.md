# LaTeX Templates

### Description

This repo is a collection of both classes (.cls) and styles/packages (.sty) I
use for various purposes (currently only have math-based ones, but more are on
the way).

### Requirements

Nothing other than a working LaTeX distribution is required, as well as all the
used packages installed too. Make sure that all the first-party packages came
bundled with your installation of LaTeX, and that you have access to the extra
fonts (like the noto fonts package).

### Usage

If you don't want to install the files to a permanent location (or you don't
know how) you can always just copy the files in the src/ directory and paste
them in the folder where you LaTeX file is (the current directory always gets
scanned)

### Install (Linux / MacOS)

To install, simply run the shell script with your directory at the root of the
git repo. It installs the classes and packages to a hidden directory in your
home folder that automatically gets sourced, so nothing else needs to be done,
and you can forever use the classes and packages without having to manually
copy them.

### Classes Included

Currently 2 classes are included: math-short and math-long. They both are meant
to be used for math assignments, but the short one looks better for assignment
problems that have short question answers (like quizes), while the long one is
better suited for longer problems which need multiple pages to solve.

### Packages Included

Currently only 1 package is included: ezmath. This package makes writing math
easier. Specifically, it focuses more on continuous mathematics and its applied
uses, such as physics. It is not focused on making discrete mathematics easier
to write.
